<!--
.. title: Homepage
.. slug: index
.. date: 2019-03-16 18:00:00 UTC
.. tags: Green,Bay,Linux,Users,Group,LUG,GreenBay,GBLUG,GBWILUG
.. link:
.. description:
.. type: text
.. hidetitle: false
-->

Welcome to the homepage of the Green Bay Linux Users group!

# What is the Green Bay Linux Users Group (GBLUG)?

This group exists to advocate for the use of the Linux operating system in Green Bay, Wisconsin, USA. Anyone from beginners who want to learn more about Linux to experts who work with it every day (and everyone in between) are welcome. From demonstrations, to Linux support, and much more, the GBLUG is your Green Bay Linux outlet.

# Where and when are the meetings?

Meetings are coordinated on the [GetTogether website](https://gettogether.community/green-bay-linux-users-group/), which is an [open source](https://github.com/GetTogetherComm/GetTogether) event manager for local communities. You can find specific and up-to-date information about the events there.

The two most common locations for the meetings are at [The Attic Corner](https://theatticcorner.com/) and the [Central Brown County Library](https://www.browncountylibrary.org/), both in downtown Green Bay.

# What's Linux?

[Linux](https://en.wikipedia.org/wiki/Linux) is a family of open source operating systems based on the Linux kernel. It is the most popular server operating system in the world and is also available as a desktop operating system, as a replacement for Windows or Mac OS X.

The GBLUG is for Linux enthusiasts of all kinds, including (but not limited to) server, desktop, IoT, and other applications.
